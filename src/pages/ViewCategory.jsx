import React from 'react'
import { useLocation, useNavigate } from "react-router-dom";
import { Button, Container, Row } from "react-bootstrap";

export default function () {
    const location = useLocation();
    const oldData = location.state.item;
    const navigate = useNavigate();
    return (
        <Container className="my-2">
            <Row className="p-3">
                <Button onClick={() => navigate("/categories")} style={{ width: "10%" }} className="m-3 btn-light fw-bold">
                    Back
                </Button>
                <div className="mt-2 text-start">
                    <h1 className="p-3">Category Name: <span className="text-primary">{oldData?.name}</span> </h1>

                </div>
            </Row>
        </Container>
    )
}
