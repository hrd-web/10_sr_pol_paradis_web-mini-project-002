import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { Link, useLocation } from "react-router-dom";
import Swal from 'sweetalert2';
import { api } from '../api/api';

export default function ViewCategory() {

    const [title, setTitle] = useState();
    const [description, setDescription] = useState("");
    const [isPublished, setIsPublished] = useState("");
    const [image, setImage] = useState("");
    const [imageUrl, setImageUrl] = useState();

    const location = useLocation();

    const handleTitleChange = (e) => {
        setTitle(e.target.value);
    };

    useEffect(() => {
        setTitle(location.state?.item.title);
        setDescription(location.state?.item.description);
        setImage(location.state?.item.image);

    }, [location]);

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    };

    const isPublishedChange = (e) => {
        setIsPublished(e.target.checked);
    };


    return (
        <div>
            <Container>
                <h1>View Article</h1>
                <Form>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" value={title} placeholder="Enter title" onChange={handleTitleChange} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" value={description} rows={3} placeholder="Enter description" onChange={handleDescriptionChange} />
                    </Form.Group>

                    <img src={image ? image : "https://www.divesupply.com/wp-content/uploads/2020/04/No-image.jpg"} alt="no image preview" style={{ height: "200px" }} />

                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                        <Form.Check
                            type="checkbox"
                            disabled
                            defaultChecked={location.state?.item.published ? "checked" : ""}
                            label="Published"
                            onChange={isPublishedChange}
                        />
                    </Form.Group>
                </Form>
            </Container>
        </div>
    )
}
