import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2';
import { api } from '../api/api';

export default function NewArticle() {

    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [isPublished, setIsPublished] = useState("");
    const [image, setImage] = useState("");
    const [imageUrl, setImageUrl] = useState();
    const handleSubmit = () => {
        api
            .post("/articles", { title, description, isPublished, image })
            .then((res) => console.log(res.data.message))
            .then(Swal.fire("Good job!", "Create Successfully!"));
    };

    const handleTitleChange = (e) => {
        setTitle(e.target.value);
    };

    useEffect(() => {
        console.log(title);
    }, [title]);

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    };

    const isPublishedChange = (e) => {
        setIsPublished(e.target.checked);
        // console.log(e.target.checked);
    };

    const handleImageChange = (e) => {
        setImageUrl(URL.createObjectURL(e.target.files[0]));

        const formData = new FormData();
        formData.append("image", e.target.files[0]);
        console.log("FormData : ", formData.get("image"));
        api.post("/images", formData).then((res) => setImage(res.data.payload.url));

    };

    return (
        <div>
            <Container>
                <h1>Add new article</h1>
                <Form>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" placeholder="Enter title" onChange={handleTitleChange} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows={3} placeholder="Enter description" onChange={handleDescriptionChange} />
                    </Form.Group>


                    <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Image file</Form.Label>
                        <Form.Control type="file" onChange={handleImageChange} />
                    </Form.Group>

                    <img src={imageUrl ?? "https://www.divesupply.com/wp-content/uploads/2020/04/No-image.jpg"} alt="no image preview" style={{ height: "200px" }} />

                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                        <Form.Check
                            type="checkbox"
                            label="Published"
                            onChange={isPublishedChange}
                        />
                    </Form.Group>

                    <Button variant='danger' as={Link} to='/'>Cancel</Button>
                    <Button variant='success' onClick={handleSubmit}>Submit</Button>
                </Form>
            </Container>
        </div>
    )
}
