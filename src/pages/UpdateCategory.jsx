import React from 'react'
import { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { api } from "../api/api";
import Swal from "sweetalert2";
import { useLocation, useNavigate } from "react-router-dom";
import '../App.css';

export default function EditCategory() {
    const [id, setId] = useState();
    const [name, setName] = useState("");
    const navigate = useNavigate();
    const location = useLocation();
    const beforeSubmit = () => {
        Swal.fire({
            title: "Updated successfully !",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, save it!",
        }).then((result) => {
            if (result.isConfirmed) {
                handleSubmit();
                navigate("/categories");
            } else {
                return;
            }
        });
    };


    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleSubmit = () => {

        api
            .put("/category/" + id, { name })
            .then((res) => Swal.fire("Your file has been Save.!", res?.data?.message, "success"));
    };

    useEffect(() => {
        setName(location.state?.item.name);
        setId(location.state?.item._id);
    }, [location]);

    return (
        <Container className="w-50 mt-5">
            <br />
            <h1>Update Category</h1>
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" value={name} placeholder="name" onChange={handleNameChange} />
                </Form.Group>
                <Button variant="primary" onClick={beforeSubmit}> Submit </Button>
            </Form>
        </Container>
    )
}
