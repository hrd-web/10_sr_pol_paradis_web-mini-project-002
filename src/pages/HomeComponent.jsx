import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { api } from '../api/api';
import CardComponent from '../components/CardComponent';

export default function HomeComponent() {

  const [data, setData] = useState([]);
  useEffect(() => {
    api.get("/articles").then((r) => {
      setData(r.data.payload);
      //console.log(r.data.payload);
    });
  }, []);

  const handleDelete = (id) => {
    const buttons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    Swal.fire({

      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
      .then((result) => {
        let message = "Your file have been deleted.";
        if (result.isConfirmed) {
          const newData = data.filter((result) => data._id !== id);
          setData(newData);
          api.delete(`/articles/${id}`).then((r) => { console.log(r.data.message) });

          window.location.reload();
        } else {
          return;
        }
      });
  };

  return (
    <div>
      <Container>
        <Row>
          <Col className='mt-5'>
            <h3>All Articles</h3>
          </Col>

          <Col className='mt-5 new-article'>
            <Button variant='light' as={Link} to="/new">New Article</Button>
          </Col>
        </Row>

        <Row>
          {data.map((item, index) => (
            <Col xs={12} sm={6} md={2} key={index}>
              <CardComponent item={item} handleDelete={handleDelete} />
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}

