import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { api } from '../api/api';
import CardCategoryComponent from '../components/CardCategoryComponent';

export default function HomeComponent() {

  const navigate = useNavigate();
  const [data, setData] = useState([]);
  useEffect(() => {
    api.get("/category").then((r) => {
      setData(r.data.payload);
      //console.log(r.data.payload);
    });
  }, []);

  const handleDelete = (id) => {
    const buttons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButton: 'You deleted it!',
      cancelButton: 'Nope!'
    })
      .then((result) => {
        let message = "Your file have been deleted.";
        if (result.isConfirmed) {
          const newData = data.filter((result) => data._id !== id);
          setData(newData);
          api.delete(`/category/${id}`).then((r) => {
            window.location.reload();
          });


        } else {
          return;
        }
      });
  };

  return (
    <div>
      <Container>
        <Row>
          <Col className='mt-5'>
            <h3>All Categories</h3>
          </Col>

          <Col className='mt-5 new-article'>
            <Button variant='light' as={Link} to="/new-category">New Categogries</Button>
          </Col>
        </Row>

        <Row>
          {data.map((item, index) => (
            <Col xs={12} sm={6} md={2} key={index}>
              <CardCategoryComponent item={item} handleDelete={handleDelete} />
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}

