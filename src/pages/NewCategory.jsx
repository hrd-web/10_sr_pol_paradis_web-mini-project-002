import axios from "axios";
// import React from 'react';
import React, { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import { api } from "../api/api";
import { useNavigate } from "react-router-dom";


export default function CreateCategory() {
    const [name, setTitle] = useState("");
    const navigate = useNavigate();
    const beforeSubmit = () => {
        Swal.fire({
            title: 'Sweet!',
            text: 'Have a nice day',
            imageUrl: 'http://www.hodgepodge.me/wp-content/uploads/2011/12/IMG_2599-580x386.jpg',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'new-categories',
        }).then((result) => {
            if (result.isConfirmed) {
                handleSubmit();
                navigate("/categories");
            } else {
                return;
            }
        });
    };

    const handleSubmit = () => {
        api
            .post("/category", { name })
    };

    const handleNameChange = (e) => {
        setTitle(e.target.value);
    };

    useEffect(() => {
        console.log(name);
    }, [name]);

    return (
        <Container className="w-50 mt-5">
            <h1 className="mt-5" >Add New Category</h1>
            <Form className=" shadow p-2 mb-2 bg-white">
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" placeholder="name" onChange={handleNameChange} />
                </Form.Group>
                <Button variant="primary" onClick={beforeSubmit}>
                    Submit
                </Button>
            </Form>
        </Container>
    );
}
