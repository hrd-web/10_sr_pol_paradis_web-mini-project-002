import React from "react";
import { Button, Card, Col, Row, Container } from "react-bootstrap";
import '../App.css';
import { useNavigate, useLocation } from "react-router-dom";

export default function CardComponent({ item, handleDelete }) {
    const navigate = useNavigate();
    const onView = () => {
        navigate("/view", { state: { item: item } });
    };

    const onUpdate = (item) => {
        navigate("/update", { state: { item: item } });
    };
    return (
        <div className="mt-2">
            <Row>
                <Card>
                    <Card.Img variant="top" src={item.image ?? "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"} />
                    <Card.Body>
                        <Card.Title className="card-p">{item.title}</Card.Title>
                        <Card.Text className="card-p">
                            {item.description.substring(0, 80,) + " ......." ?? "No data."}
                        </Card.Text>
                        <Col lg={12}>
                            <Button variant="warning" onClick={onView} className="w-100 mt-2">View</Button>
                        </Col>
                        <Col lg={12}>
                            <Button variant="primary" onClick={() => onUpdate(item)} className="w-100 mt-2">Update</Button>
                        </Col>
                        <Col lg={12}>
                            <Button variant="danger" className="w-100 mt-2" onClick={() => handleDelete(item._id)}>Delete</Button>
                        </Col>
                    </Card.Body>
                </Card>

            </Row>
        </div>
    );
}
