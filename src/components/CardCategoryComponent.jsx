import React from "react";
import { Button, Card, Col, Row, Container } from "react-bootstrap";
import '../App.css';
import { useNavigate, useLocation } from "react-router-dom";

export default function CardComponent({ item, handleDelete }) {
    const navigate = useNavigate();
    const onView = () => {
        navigate("/view-category", { state: { item: item } });
    };

    const onUpdate = (item) => {
        navigate("/update-category", { state: { item: item } });
    };
    return (
        <div className="mt-2">
            <Row>
                <Card>
                    <Card.Body>
                        <Card.Title className="card-p">{item.name}</Card.Title>
                        <Col lg={12}>
                            <Button variant="warning" onClick={() => onView(item)} className="w-100 mt-2">View</Button>
                        </Col>
                        <Col lg={12}>
                            <Button variant="primary" onClick={() => onUpdate(item)} className="w-100 mt-2">Update</Button>
                        </Col>
                        <Col lg={12}>
                            <Button variant="danger" className="w-100 mt-2" onClick={() => handleDelete(item._id)}>Delete</Button>
                        </Col>
                    </Card.Body>
                </Card>

            </Row>
        </div>
    );
}
