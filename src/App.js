import './App.css';
import { Route, Routes } from 'react-router-dom';
import NavbarComponent from './components/NavbarComponent'
import HomeComponent from './pages/HomeComponent'
import NewArticle from './pages/NewArticle'
import UpdateArticle from './pages/UpdateArticle'
import ViewArticle from './pages/ViewArticle'
import NewCategory from './pages/NewCategory'
import UpdateCategory from './pages/UpdateCategory'
import ViewCategory from './pages/ViewCategory'

import CategoriesComponent from './pages/CategoriesComponent'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function App() {
  return (
      <div className="App">
        <NavbarComponent/>

        <Routes>
          <Route path='/' element={<HomeComponent/>}/>
          <Route path='/new' element={<NewArticle/>}/>
          <Route path='/new-category' element={<NewCategory/>}/>
          <Route path='/update' element={<UpdateArticle/>}/>
          <Route path='/view' element={<ViewArticle/>}/>
          <Route path='/categories' element={<CategoriesComponent/>}/>
          <Route path='/update-category' element={<UpdateCategory/>}/>
          <Route path='/view-category' element={<ViewCategory/>}/>
        </Routes>
      </div>  
  );
}

